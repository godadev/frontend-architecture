import { useState, useEffect } from "react";

interface DarkModeHook {
  darkMode: boolean;
  toggleDarkMode: () => void;
}

const useDarkMode = (): DarkModeHook => {
  const [darkMode, setDarkMode] = useState(false);

  useEffect(() => {
    const className = "dark";
    const element = document.documentElement;

    if (darkMode) {
      element.classList.add(className);
    } else {
      element.classList.remove(className);
    }
  }, [darkMode]);

  const toggleDarkMode = () => setDarkMode(!darkMode);

  return { darkMode, toggleDarkMode };
};

export default useDarkMode;
