import useGetAllPokemon from "../../infrastructure/api/pokeAPI/query";
import { PokemonList } from "../../shared/types/pokemon";
import useDarkMode from "../hooks/useDarkMode";
import DarkModeSwitch from "./DarkModeSwitch";

const Home: React.FC = () => {
  const { pokemon } = useGetAllPokemon();
  const { darkMode, toggleDarkMode } = useDarkMode();

  return (
    <div className="flex justify-center items-center bg-white dark:bg-gray-800">
      <div>
        <DarkModeSwitch isDarkMode={darkMode} toggleDarkMode={toggleDarkMode} />
        <div className="text-5xl font-bold text-center text-blue-600 py-4 dark:text-gray-200">
          Pokemon List
        </div>
        <ul className="flex flex-col items-center">
          {pokemon.map((p: PokemonList) => (
            <div
              key={p.name}
              className="bg-white dark:bg-gray-700 rounded-lg shadow-md m-2 p-4"
            >
              <li className="text-3xl text-green-700 dark:text-green-300 font-semibold">
                {p.name}
              </li>
              <li className="text-sm text-gray-600 dark:text-gray-300">
                {p.url}
              </li>
            </div>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Home;
