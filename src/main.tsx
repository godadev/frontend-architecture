import ReactDOM from "react-dom/client";
import "./ui/styles/index.css";
import RouterDomProvider from "./infrastructure/providers/router.provider";
import ReduxProvider from "./infrastructure/providers/redux.provider";
import QueryProvider from "./infrastructure/providers/query.provider";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <ReduxProvider>
    <QueryProvider>
      <RouterDomProvider />
    </QueryProvider>
  </ReduxProvider>
);
