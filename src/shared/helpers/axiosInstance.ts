import axios from "axios";

const httpConfig = {
  baseURL: "https://pokeapi.co/api/v2/",
};

const axiosInstance = axios.create(httpConfig);

export default axiosInstance;
