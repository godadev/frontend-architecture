import { createBrowserRouter } from "react-router-dom";
import HomeView from "../ui/views/HomeView";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomeView />,
  },
  {
    path: "/about",
    element: <h1>About</h1>,
  },
  {
    path: "/contact",
    element: <h1>Contact</h1>,
  },
]);

export default router;
