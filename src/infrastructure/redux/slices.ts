import { combineReducers } from "redux";
import pokeSlice from "../api/pokeAPI/slices/pokeSlice";

export default combineReducers({
  poke: pokeSlice,
});
