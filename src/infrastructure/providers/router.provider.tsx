import { RouterProvider } from "react-router-dom";
import router from "../../routers/routes";

const RouterDomProvider = () => {
  return <RouterProvider router={router} />;
};

export default RouterDomProvider;
