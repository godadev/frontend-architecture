import { useQuery } from "@tanstack/react-query";
import { getAllPokemon } from "../network";

const useGetAllPokemon = () => {
  const { data, isLoading, isError, error, isFetching } = useQuery({
    queryKey: ["getAllPokemon"],
    queryFn: getAllPokemon,
    retry: 1,
  });

  const pokemon = data?.data.results || [];
  return { pokemon, isLoading, isError, error, isFetching };
};

export default useGetAllPokemon;
