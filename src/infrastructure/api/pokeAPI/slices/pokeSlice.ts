import { createSlice } from "@reduxjs/toolkit";

interface Pokemon {
  name: string;
}

interface PokeState {
  pokemons: Pokemon[];
  error: string | null;
}

const initialState: PokeState = {
  pokemons: [],
  error: null,
};

const pokeSlice = createSlice({
  name: "poke",
  initialState,
  reducers: {
    pokeSuccess: (state, action) => {
      state.pokemons = action.payload;
    },
    pokeFailure: (state, action) => {
      state.error = action.payload;
    },
  },
});

export const { pokeSuccess, pokeFailure } = pokeSlice.actions;
export default pokeSlice.reducer;
