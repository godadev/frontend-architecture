import { AxiosInstance } from "axios";
import pokeServices from "../services";

const pokeNetwork = (client: AxiosInstance) => ({
  getAllPokemon: async () => client.get(pokeServices().baseUrl),
});

export default pokeNetwork;
