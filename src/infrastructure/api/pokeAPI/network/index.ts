import axiosInstance from "../../../../shared/helpers/axiosInstance";
import pokeNetwork from "./pokeNetwork";

const pokeNetworkInstance = pokeNetwork(axiosInstance);

export const getAllPokemon = pokeNetworkInstance.getAllPokemon;
